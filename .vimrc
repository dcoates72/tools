setlocal spell spelllang=en_us
set ts=4
set sw=4
set expandtab

command Latex execute "silent !pdflatex % > /dev/null" | redraw!
:map <F2> :Latex <CR>

" Bundle Scripts-----------------------------
if &compatible
  set nocompatible               " Be iMproved
endif

let g:LustyJugglerSuppressRubyWarning = 1

" Required:
set runtimepath+=/home/dcoates/.vim/bundle/neobundle.vim/

" Required:
call neobundle#begin(expand('/home/dcoates/.vim/bundle'))

" Let NeoBundle manage NeoBundle
" Required:
NeoBundleFetch 'Shougo/neobundle.vim'

" Add or remove your Bundles here:
NeoBundle 'Shougo/neosnippet.vim'
NeoBundle 'Shougo/neosnippet-snippets'
NeoBundle 'tpope/vim-fugitive'
NeoBundle 'ctrlpvim/ctrlp.vim'
NeoBundle 'flazz/vim-colorschemes'
NeoBundle 'chrisbra/csv.vim'
NeoBundle 'dhruvasagar/vim-dotoo'

NeoBundle 'jceb/vim-orgmode'
NeoBundle 'tpope/vim-speeddating'
"NeoBundle 'vim-airline/vim-airline-themes'
"NeoBundle 'vim-airline/vim-airline'
"
NeoBundle 'Lokaltog/powerline', {'rtp':'powerline/bindings/vim'}

NeoBundle 'scrooloose/nerdtree'
NeoBundle 'jistr/vim-nerdtree-tabs'

NeoBundle 'vim-scripts/LustyJuggler'

NeoBundle 'itchyny/calendar.vim'

" You can specify revision/branch/tag.
NeoBundle 'Shougo/vimshell', { 'rev' : '3787e5' }

" NeoBundle 'ayu-theme/ayu-vim'

" Required:
call neobundle#end()

" Required:
:filetype plugin indent on

" If there are uninstalled bundles found on startup,
" this will conveniently prompt you to install them.
NeoBundleCheck
"End NeoBundle Scripts-------------------------

" Customize dotoo:
let g:dotoo#agenda#files = ['/home/dcoates/Dropbox/todo/todo.org',  '/home/dcoates/Dropbox/todo/notes.org','/home/dcoates/Dropbox/todo/personal.org','/home/dcoates/Dropbox/todo/refile.org']
let g:dotoo#capture#refile = '/home/dcoates/Dropbox/todo/refile.org'

" set termguicolors     " enable true colors support
let ayucolor="light"  " for light version of theme
let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme ayu

let mapleader = ","

set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256

" A lot of stuff snagged from https://github.com/mitechie/pyvim/blob/master/.vimrc

" map the various registers to a leader shortcut for pasting from them
nmap <leader>0 "0p
nmap <leader>1 "1p
nmap <leader>2 "2p
nmap <leader>3 "3p
nmap <leader>4 "4p
nmap <leader>5 "5p
nmap <leader>6 "6p
nmap <leader>7 "7p
nmap <leader>8 "8p
nmap <leader>9 "9p

" shortcuts for copying to clipboard
nmap <leader>y "*y
" shortcuts for copying from sys clipboard
nmap <leader>p "+p

" ==================================================
" Windows / Splits
" ==================================================

" ctrl-jklm  changes to that split
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" and lets make these all work in insert mode too ( <C-O> makes next cmd
" happen as if in command mode )
imap <C-W> <C-O><C-W>

" use - and + to resize horizontal splits
map - <C-W>-
map + <C-W>+

" and for vsplits with alt-< or alt->
map <M-,> <C-W>>
map <M-.> <C-W><

" F2 close current window (commonly used with my F1/F3 functions)
noremap <f2> <Esc>:close<CR><Esc>

" lusty-juggler
" http://www.vim.org/scripts/script.php?script_id=2050
nmap <silent> <Leader>b :LustyJuggler<CR>

" NERDTree
" http://www.vim.org/scripts/script.php?script_id=1658
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']
map <leader>a :NERDTree<CR>

map <leader>dt :set ft=dotoo<CR>

"https://dougblack.io/words/a-good-vimrc.html
" space open/closes folds
nnoremap <space> za
let g:dotoo#agenda#files = ['/home/dcoates/Dropbox/todo/todo.org',  '/home/dcoates/Dropbox/todo/notes.org','/home/dcoates/Dropbox/todo/refile.org']
let g:dotoo#capture#refile = '/home/dcoates/Dropbox/todo/refile.org'

" set termguicolors     " enable true colors support
let ayucolor="light"  " for light version of theme
let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme ayu

let mapleader = ","

set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256

" A lot of stuff snagged from https://github.com/mitechie/pyvim/blob/master/.vimrc

" map the various registers to a leader shortcut for pasting from them
nmap <leader>0 "0p
nmap <leader>1 "1p
nmap <leader>2 "2p
nmap <leader>3 "3p
nmap <leader>4 "4p
nmap <leader>5 "5p
nmap <leader>6 "6p
nmap <leader>7 "7p
nmap <leader>8 "8p
nmap <leader>9 "9p

" shortcuts for copying to clipboard
nmap <leader>y "*y
" shortcuts for copying from sys clipboard
nmap <leader>p "+p

" ==================================================
" Windows / Splits
" ==================================================

" ctrl-jklm  changes to that split
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" and lets make these all work in insert mode too ( <C-O> makes next cmd
" happen as if in command mode )
imap <C-W> <C-O><C-W>

" use - and + to resize horizontal splits
map - <C-W>-
map + <C-W>+

" and for vsplits with alt-< or alt->
map <M-,> <C-W>>
map <M-.> <C-W><

" F2 close current window (commonly used with my F1/F3 functions)
noremap <f2> <Esc>:close<CR><Esc>

" lusty-juggler
" http://www.vim.org/scripts/script.php?script_id=2050
nmap <silent> <Leader>b :LustyJuggler<CR>

" NERDTree
" http://www.vim.org/scripts/script.php?script_id=1658
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']
map <leader>a :NERDTree<CR>

map <leader>dt :set ft=dotoo<CR>

"https://dougblack.io/words/a-good-vimrc.html
" space open/closes folds
nnoremap <space> za
let g:dotoo#agenda#files = ['/home/dcoates/Dropbox/todo/todo.org',  '/home/dcoates/Dropbox/todo/notes.org','/home/dcoates/Dropbox/todo/refile.org']
let g:dotoo#capture#refile = '/home/dcoates/Dropbox/todo/refile.org'

" set termguicolors     " enable true colors support
let ayucolor="light"  " for light version of theme
let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme ayu

let mapleader = ","

set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256

" A lot of stuff snagged from https://github.com/mitechie/pyvim/blob/master/.vimrc

" map the various registers to a leader shortcut for pasting from them
nmap <leader>0 "0p
nmap <leader>1 "1p
nmap <leader>2 "2p
nmap <leader>3 "3p
nmap <leader>4 "4p
nmap <leader>5 "5p
nmap <leader>6 "6p
nmap <leader>7 "7p
nmap <leader>8 "8p
nmap <leader>9 "9p

" shortcuts for copying to clipboard
nmap <leader>y "*y
" shortcuts for copying from sys clipboard
nmap <leader>p "+p

" ==================================================
" Windows / Splits
" ==================================================

" ctrl-jklm  changes to that split
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" and lets make these all work in insert mode too ( <C-O> makes next cmd
" happen as if in command mode )
imap <C-W> <C-O><C-W>

" use - and + to resize horizontal splits
map - <C-W>-
map + <C-W>+

" and for vsplits with alt-< or alt->
map <M-,> <C-W>>
map <M-.> <C-W><

" F2 close current window (commonly used with my F1/F3 functions)
noremap <f2> <Esc>:close<CR><Esc>

" lusty-juggler
" http://www.vim.org/scripts/script.php?script_id=2050
nmap <silent> <Leader>b :LustyJuggler<CR>

" NERDTree
" http://www.vim.org/scripts/script.php?script_id=1658
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']
map <leader>a :NERDTree<CR>

map <leader>dt :set ft=dotoo<CR>

"https://dougblack.io/words/a-good-vimrc.html
" space open/closes folds
nnoremap <space> za
let g:dotoo#agenda#files = ['/home/dcoates/Dropbox/todo/todo.org',  '/home/dcoates/Dropbox/todo/notes.org','/home/dcoates/Dropbox/todo/refile.org']
let g:dotoo#capture#refile = '/home/dcoates/Dropbox/todo/refile.org'

" set termguicolors     " enable true colors support
let ayucolor="light"  " for light version of theme
let ayucolor="mirage" " for mirage version of theme
let ayucolor="dark"   " for dark version of theme
colorscheme ayu

let mapleader = ","

set  rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256

" A lot of stuff snagged from https://github.com/mitechie/pyvim/blob/master/.vimrc

" map the various registers to a leader shortcut for pasting from them
nmap <leader>0 "0p
nmap <leader>1 "1p
nmap <leader>2 "2p
nmap <leader>3 "3p
nmap <leader>4 "4p
nmap <leader>5 "5p
nmap <leader>6 "6p
nmap <leader>7 "7p
nmap <leader>8 "8p
nmap <leader>9 "9p

" shortcuts for copying to clipboard
nmap <leader>y "+y
" shortcuts for copying from sys clipboard
nmap <leader>p "+p

" ==================================================
" Windows / Splits
" ==================================================

" ctrl-jklm  changes to that split
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-l> <c-w>l
map <c-h> <c-w>h

" and lets make these all work in insert mode too ( <C-O> makes next cmd
" happen as if in command mode )
imap <C-W> <C-O><C-W>

" use - and + to resize horizontal splits
map - <C-W>-
map + <C-W>+

" and for vsplits with alt-< or alt->
map <M-,> <C-W>>
map <M-.> <C-W><

" F2 close current window (commonly used with my F1/F3 functions)
noremap <f2> <Esc>:close<CR><Esc>

" lusty-juggler
" http://www.vim.org/scripts/script.php?script_id=2050
nmap <silent> <Leader>b :LustyJuggler<CR>

" NERDTree
" http://www.vim.org/scripts/script.php?script_id=1658
let NERDTreeIgnore = ['\.pyc$', '\.pyo$']
map <leader>a :NERDTree<CR>

map <leader>dt :set ft=dotoo<CR>

let g:calendar_google_calendar = 1
let g:calendar_google_task = 1

"https://dougblack.io/words/a-good-vimrc.html
" space open/closes folds
nnoremap <space> za

:nnoremap <F2> "=strftime("%Y-%m-%d %T")<CR>P
:inoremap <F2> <C-R>=strftime("%Y-%m-%d %T")<CR>
