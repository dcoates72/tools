CURDIR = $(shell pwd)

setup:
	ln -s $(CURDIR)/.bashrc ../.bashrc
	ln -s $(CURDIR)/.vimrc ../.vimrc
	ln -s $(CURDIR)/.gitconfig ../.gitconfig

clean:
	rm ../.bashrc
	rm ../.vimrc
	rm ../.gitconfig
